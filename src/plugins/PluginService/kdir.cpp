﻿#include "kdir.h"

namespace KServer {

//读取主项目Kylin-installer_new路径
QString GetKylinInstallPath()
{
#if 0
    QDir dir(QDir::currentPath());
    dir.cdUp();
    dir.cdUp();
    dir.cdUp();

    QString path = dir.path();
    path.append("/kylin-os-installer");
#else
    QString path = "/usr/share/kylin-os-installer/";
#endif
    return (QString)path;
}

}
