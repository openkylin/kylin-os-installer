#ifndef LANGUAGEFRAME_H
#define LANGUAGEFRAME_H

#include <QObject>
#include <QWidget>
#include <QLabel>
#include <QComboBox>
#include "../PluginService/ui_unit/comboxdelegate.h"
#include "../PluginService/ui_unit/comboxlistmodel.h"
#include "../PluginService/ui_unit/middleframemanager.h"
#include <QEvent>
#include <QPushButton>
#include "pushbuttoncheck.h"
using namespace KServer;
namespace KInstaller {


class LanguageFrame : public MiddleFrameManager
{
    Q_OBJECT
public:
    explicit LanguageFrame(QWidget *parent = nullptr);

public:
    void initUI();                              //加载界面控件
    void initAllConnect();                      //信号槽连接
    void translateStr();                        //更新需要翻译的对象字符串
    void addStyleSheet();                       //添加窗口样式
    void initLanguageBox();                     //设置下拉框model
    void initBtn();

    void readSettingIni();                      //读写systemsetting.ini文件
    void writeSettingIni();                     //读写systemsetting.ini文件
    void setBoardTabOrder();

signals:
    void signalPageChanged(int flag);
public slots:
    void changeLanguageEn();
    void changeLanguageZh();
    void clickNextButton();

private:
    void changeEvent(QEvent *event) override;       //改变语言事件
    void keyPressEvent(QKeyEvent *event);
private:

    QComboBox* m_languageBox;
    QLabel* m_mainTitle;

    QString m_curSystenlocal;               //当前选中的语言名称
    ComboxListModel* m_model;               //model
    ItemLanguage m_comboxItem;              //列表项

    QLabel* m_kylinpng;
    QLabel* m_titleOS;
    QLabel* m_littleOS;

    PushButtonCheck* m_ZHCNBtn;
    PushButtonCheck* m_ENUSBtn;

};
}
#endif // LANGUAGEFRAME_H
