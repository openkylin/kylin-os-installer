﻿#include "pushbuttoncheck.h"
#include <QHBoxLayout>

PushButtonCheck::PushButtonCheck(QString str, QWidget *parent) : QPushButton(parent)
{
    QHBoxLayout* layout = new QHBoxLayout(this);
    m_labelpix = new QLabel(this);
    m_labelpix->setPixmap(QPixmap(":/res/png/clicked.svg").scaled(24, 24));
    this->setLayout(layout);
    layout->setAlignment(this, Qt::AlignLeft);
    this->setText(str);
    layout->addStretch();
    layout->addWidget(m_labelpix);

    m_labelpix->setVisible(false);
}
