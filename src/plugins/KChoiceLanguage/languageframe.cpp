﻿#include "languageframe.h"
#include <QWidget>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QDebug>
#include <QProcess>
#include <QApplication>

#include <QFile>
#include <QTextStream>
#include "../PluginService/klinguist.h"
#include "../PluginService/ksystemenv.h"
#include "../PluginService/ksystemsetting_unit.h"
#include "../PluginService/kdir.h"
#include "../PluginService/ui_unit/keyeventcontrol.h"
#include "../PluginService/ui_unit/xrandrobject.h"

using namespace KServer;

namespace KInstaller {

LanguageFrame::LanguageFrame(QWidget *parent) : MiddleFrameManager(parent)
{

    initUI();
    addStyleSheet();
    initAllConnect();
    initBtn();
    setBoardTabOrder();
    this->setFocus();
}

void LanguageFrame::initUI()
{
    QGridLayout* gLayoutl = new QGridLayout();
    gLayoutl->setContentsMargins(0, 0, 0, 0);
    m_Widget->setLayout(gLayoutl);
    gLayoutl->setColumnStretch(0, 3);
    gLayoutl->setColumnStretch(1, 2);
    gLayoutl->setColumnStretch(2, 3);

    gLayoutl->addItem(new QSpacerItem(10, 40, QSizePolicy::Expanding, QSizePolicy::Preferred), 0, 1, 1, 1);
    m_mainTitle = new QLabel();
    m_mainTitle->setObjectName("mainTitle");

    gLayoutl->addWidget(m_mainTitle, 1, 1, 1, 1, Qt::AlignCenter);
    gLayoutl->addItem(new QSpacerItem(10, 40, QSizePolicy::Expanding, QSizePolicy::Preferred), 2, 1, 1, 1);

    m_ZHCNBtn = new PushButtonCheck("中文(简体)");
    m_ZHCNBtn->setObjectName("ZHCNBtn");
    m_ZHCNBtn->setCheckable(true);

    m_ENUSBtn = new PushButtonCheck("English");
    m_ENUSBtn->setObjectName("ENUSBtn");
    m_ENUSBtn->setCheckable(true);


    gLayoutl->addWidget(m_ZHCNBtn, 3, 1, 1, 1, Qt::AlignCenter);
    gLayoutl->addItem(new QSpacerItem(8, 24, QSizePolicy::Expanding, QSizePolicy::Minimum), 4, 1, 1, 1);

    gLayoutl->addWidget(m_ENUSBtn, 5, 1, 1, 1, Qt::AlignCenter);
    gLayoutl->setRowStretch(6, 1);
    translateStr();
}

void LanguageFrame::setBoardTabOrder()
{
    QWidget::setTabOrder(m_ZHCNBtn, m_ENUSBtn);
    QWidget::setTabOrder(m_ENUSBtn, m_nextBtn);
}

void LanguageFrame::initBtn()
{
    m_curSystenlocal = QString(getenv("LANGUAGE")).section('.',0,0);
    WriteSettingToIni("config", "language", m_curSystenlocal);
    if (m_curSystenlocal == "en_US") {
        m_ENUSBtn->setLabelShow(true);
        m_ENUSBtn->setChecked(true);
    } else {
        m_ZHCNBtn->setLabelShow(true);
        m_ZHCNBtn->setChecked(true);
    }
}

void LanguageFrame::changeLanguageEn()
{
    QTranslator* curTranslator = new QTranslator;
    QString langPath = QString("");
    langPath = GetKylinInstallPath() + "/language/";

    //清理原来的翻译文件
    if(!curTranslator->isEmpty())
    {
        qApp->removeTranslator(curTranslator);
        delete curTranslator;
    }
    //加载新的翻译文件
    m_curSystenlocal = "en_US";
    if (curTranslator->load(langPath + m_curSystenlocal + ".qm")) {
        qApp->installTranslator(curTranslator);
    }
    m_ZHCNBtn->setLabelShow(false);
    m_ENUSBtn->setLabelShow(true);

    unsetenv("LANG");
     setenv("LANG", "en_US.UTF-8",1);
    unsetenv("LANGUAGE");
    setenv("LANGUAGE", "en_US.UTF-8",1);
    WriteSettingToIni("config", "language", m_curSystenlocal);
}

void LanguageFrame::changeLanguageZh()
{
    QTranslator* curTranslator = new QTranslator;
    QString langPath = QString("");
    langPath = GetKylinInstallPath() + "/language/";
    //清理原来的翻译文件
    if(!curTranslator->isEmpty())
    {
        qApp->removeTranslator(curTranslator);
        delete curTranslator;
    }
    //安装新的翻译
    m_curSystenlocal = "zh_CN";
    if (curTranslator->load(langPath + m_curSystenlocal + ".qm")) {
        qApp->installTranslator(curTranslator);
    }
    m_ENUSBtn->setLabelShow(false);
    m_ZHCNBtn->setLabelShow(true);
    unsetenv("LANG");
    setenv("LANG", "zh_CN.UTF-8",1);
    unsetenv("LANGUAGE");
    setenv("LANGUAGE", "zh_CN.UTF-8",1);
    WriteSettingToIni("config", "language", m_curSystenlocal);
}

void LanguageFrame::initLanguageBox()
{
    m_languageBox->clear();
    m_languageBox->setInsertPolicy(QComboBox::InsertAtBottom);
    m_model = new ComboxListModel(m_languageBox);
    m_languageBox->setModel(m_model);
//    ComboxDelegate* delegate = new ComboxDelegate;
    ComboxDelegate delegate;
    m_languageBox->setItemDelegate(&delegate);
    m_curSystenlocal = QString(getenv("LANGUAGE")).section('.',0,0);
    QModelIndex index = m_model->getIndex(m_curSystenlocal);
    if (index.isValid()) {
        m_comboxItem = m_model->getItemLanguage(index);
        m_languageBox->setCurrentIndex(index.row());
    }
    m_languageBox->setMaxVisibleItems(5);
}

void LanguageFrame::initAllConnect()
{
//    connect(m_languageBox, static_cast< void ( QComboBox::* )( int ) >(&QComboBox::currentIndexChanged), this, &LanguageFrame::changeLanguage);
    connect(m_ZHCNBtn, &PushButtonCheck::clicked, this, &LanguageFrame::changeLanguageZh);
    connect(m_ENUSBtn, &PushButtonCheck::clicked, this, &LanguageFrame::changeLanguageEn);
}



void LanguageFrame::changeEvent(QEvent *event)
{
    if (event->type() == QEvent::LanguageChange) {
        translateStr();
    } else {
        QWidget::changeEvent(event);
    }
}

void LanguageFrame::addStyleSheet()
{
    QFile file(":/res/qss/KChoiceLanguage.css");
    file.open(QFile::ReadOnly);
    QTextStream filetext(&file);
    QString stylesheet = QString("");
    stylesheet = filetext.readAll();
    file.close();

    this->setStyleSheet(stylesheet);
}

void LanguageFrame::translateStr()
{
    m_mainTitle->setText(tr("Select Language"));
//    m_titleOS->setText(tr("Welcome to use Galaxy Kylin desktop operating system"));
//    m_littleOS->setText(tr("KYLIN OS V10.1.0.4"));
    m_nextBtn->setText(tr("Next"));
}

void LanguageFrame::clickNextButton()
{
    emit signalStackPageChanged(1);
}

void LanguageFrame::keyPressEvent(QKeyEvent *event)
{
    this->setFocus();
    qDebug() << event->key() << "key pressed!";
    qDebug() << Q_FUNC_INFO << "keyPressEvent";
    if(event->key() == Qt::Key_Up)
    {
        //qDebug() << "up key pressed!";
        if(m_ZHCNBtn->isChecked() == true)
            changeLanguageEn();
        else if(m_ENUSBtn->isChecked() == true)
            changeLanguageZh();
        else
            changeLanguageZh();
    }
    else if(event->key() == Qt::Key_Down)
    {
        //qDebug() << "down key pressed!";
        if(m_ZHCNBtn->isChecked() == true)
            changeLanguageEn();
        else if(m_ENUSBtn->isChecked() == true)
            changeLanguageZh();
        else
            changeLanguageEn();
    }
    else if(event->key() == Qt::Key_Enter)
    {
        emit enterpressed();
    }
    else if(event->key() == Qt::Key_Return)
    {
        emit enterpressed();
    }
    else if(event->key() == Qt::Key_Backspace)
    {
        emit backspacepressed();
    }
    else
        QWidget::keyPressEvent(event);
}

}
