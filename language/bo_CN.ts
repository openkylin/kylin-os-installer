<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>KInstaller::CreatePartitionFrame</name>
    <message>
        <source>OK</source>
        <translation>གཏན་འཁེལ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>close</source>
        <translation>ཁ་རྒྱག</translation>
    </message>
    <message>
        <source>Size(MiB)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>End of this space</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Create Partition</source>
        <translation>བགོ་ཁུལ་གསར་འཛུགས།</translation>
    </message>
    <message>
        <source>Root partition size is greater than 15GiB, 
but Huawei machines require greater than 25GiB.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Location for the new partition</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Logical</source>
        <translation>གཏན་ཚིགས།</translation>
    </message>
    <message>
        <source>Beginning of this space</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Mount point</source>
        <translation>འགེལ་ཚེག：</translation>
    </message>
    <message>
        <source>Primary</source>
        <translation>བདག་པོ།</translation>
    </message>
    <message>
        <source>Type for the new partition:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Recommended efi partition size is between 256MiB and 2GiB.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Used to:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Recommended boot partition size is between 500MiB and 2GiB.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Mount point starts with '/'</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>KInstaller::ModifyPartitionFrame</name>
    <message>
        <source>OK</source>
        <translation>གཏན་འཁེལ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>close</source>
        <translation>ཁ་རྒྱག</translation>
    </message>
    <message>
        <source>Format partition.</source>
        <translation>རྣམ་གཞག་ཅན་གྱི་བགོ་ཁུལ།</translation>
    </message>
    <message>
        <source>Mount point</source>
        <translation>འགེལ་ཚེག：</translation>
    </message>
    <message>
        <source>Modify Partition</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>unused</source>
        <translation>སྤྱད་ཚར།</translation>
    </message>
    <message>
        <source>Used to:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Mount point starts with '/'</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>KServer::EncryptSetFrame</name>
    <message>
        <source>OK</source>
        <translation>གཏན་འཁེལ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>close</source>
        <translation>ཁ་རྒྱག</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ</translation>
    </message>
    <message>
        <source>Please keep your password properly.If you forget it,
you will not be able to access the disk data.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>confirm password:</source>
        <translation>གཏན་འཁེལ་གསང་གྲངས།</translation>
    </message>
    <message>
        <source>password:</source>
        <translation>གསང་ཨང་།</translation>
    </message>
    <message>
        <source>Two password entries are inconsistent!</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>KServer::MessageBox</name>
    <message>
        <source>OK</source>
        <translation>གཏན་འཁེལ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>close</source>
        <translation>ཁ་རྒྱག</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ</translation>
    </message>
</context>
<context>
    <name>KInstaller::TableWidgetView</name>
    <message>
        <source>no</source>
        <translation>མིན།</translation>
    </message>
    <message>
        <source>yes</source>
        <translation>ཡིན།</translation>
    </message>
    <message>
        <source>add </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Create partition table</source>
        <translation>བགོ་ཁུལ་གསར་འཛུགས།</translation>
    </message>
    <message>
        <source>change </source>
        <translation>བསྒྱུར་བ།</translation>
    </message>
    <message>
        <source>delete </source>
        <translation>འདོར་བ།</translation>
    </message>
    <message>
        <source>freespace</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>wd</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>good</source>
        <translation>བཟང་པོ།</translation>
    </message>
    <message>
        <source>size</source>
        <translation>ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <source>type</source>
        <translation> རིགས་རྣམ།</translation>
    </message>
    <message>
        <source>used</source>
        <translation>སྤྱད་ཚར།</translation>
    </message>
    <message>
        <source>kylin-data</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>No capital letter errors</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Format %1 partition, %2
</source>
        <translation>རྣམ་གཞག་ཅན་གྱི་བགོ་ཁུལ།</translation>
    </message>
    <message>
        <source>other</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The password contains more than %1 characters of the same class consecutively</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The password contains words from the real name of the user in some form</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The password contains less than %1 uppercase letters</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The password contains less than %1 lowercase letters</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Fatal failure</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The password contains forbidden words in some form</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>No password supplied</source>
        <translation>གསང་ཨང་མཁོ་འདོན་བྱས་མེད་པ།</translation>
    </message>
    <message>
        <source>Too short</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The password is shorter than %1 characters</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>No special character error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>No number error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The password contains the user name in some form</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The configuration file is malformed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Show debug informations</source>
        <translation>ཚོད་སྒྲིག་ཆ་འཕྲིན་འཆར་བ།</translation>
    </message>
    <message>
        <source>New Partition Table %1
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The password fails the dictionary check</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Swap partition</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>mounted</source>
        <translation>བཀལ་ཟིན་པ།</translation>
    </message>
    <message>
        <source>Bad integer value</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Password generation failed - required entropy too low for settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The password contains less than %1 character classes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Is empty</source>
        <translation>སྟོང་པ་ཡིན།</translation>
    </message>
    <message>
        <source>%1 partition mountPoint %2
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Create new partition %1,%2,%3
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Setting %s is not of string type</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Too long</source>
        <translation> རིང་དྲགས་པ།</translation>
    </message>
    <message>
        <source>The password is too similar to the old one</source>
        <translation>གསང་ཨང་གསར་པ་དང་གསང་ཨང་རྙིང་པ་འདྲ་དྲགས་པ།</translation>
    </message>
    <message>
        <source>Too weak</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The password is too short</source>
        <translation>གསང་ཨང་གསར་པ་ཐུང་དྲགས་པ།</translation>
    </message>
    <message>
        <source>device</source>
        <translation>སྒྲིག་ཆས།(_D)</translation>
    </message>
    <message>
        <source>The password contains too few non-alphanumeric characters</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>format</source>
        <translation>རྣམ་གཞག</translation>
    </message>
    <message>
        <source>Delete partition %1
</source>
        <translation>བགོ་ཁུལ་བསུབ་པ།</translation>
    </message>
    <message>
        <source>lenovo</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The password contains too long of a monotonic character sequence</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>system</source>
        <translation>རྒྱུད་ཁོངས།</translation>
    </message>
    <message>
        <source>unused</source>
        <translation>སྤྱད་ཚར།</translation>
    </message>
    <message>
        <source>The password is the same as the old one</source>
        <translation>གསང་ཨང་གསར་པ་དང་གསང་ཨང་རྙིང་པ་གཅིག་པ་རེད།</translation>
    </message>
    <message>
        <source>The password contains too many same characters consecutively</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The password contains too many characters of the same class consecutively</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The password is just rotated old one</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>samsung</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The password is a palindrome</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>seagate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Bad integer value of setting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The password contains monotonic sequence longer than %1 characters</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Setting is not of string type</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Unknown setting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Freespace</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Memory allocation error</source>
        <translation>ནང་གསོག་བགོ་འགྲེམས་ཀྱི་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <source>The password contains too few digits</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The password contains less than %1 non-alphanumeric characters</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The password contains more than %1 same characters consecutively</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Unknown error</source>
        <translation>གསང་ཨང་རྙོག་འཛིང་ཚད་བླང་བྱ་དང་མི་མཐུན་པ།</translation>
    </message>
    <message>
        <source>The password contains too few uppercase letters</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The password contains too few lowercase letters</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Create new partition %1,%2
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Setting is not of integer type</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Opening the configuration file failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>No lowercase letter errors</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The password contains less than %1 digits</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Invalid character error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Cannot obtain random numbers from the RNG device</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Setting %s is not of integer type</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Format partition %1,%2,%3
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The password does not contain enough character classes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Memory allocation error when setting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The password differs with case changes only</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The first character is wrong</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>KInstaller::MainPartFrame</name>
    <message>
        <source>This machine not support EFI partition
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Next</source>
        <translation>རྗེས་མ།</translation>
    </message>
    <message>
        <source>Repeated mountpoint
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Boot partition too small
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Formatted the whole disk</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>BootLoader method %1 inconsistent with the disk partition table 
type %2.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Coexist Install</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Confirm Full Installation</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Custom install</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Boot filesystem invalid
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Boot partition not in the first partition
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Confirm the above operations</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>No Efi partition
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>InvalidId
</source>
        <translation>ནུས་མེད།</translation>
    </message>
    <message>
        <source>No backup partition
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Choose Installation Method</source>
        <translation>སྒྲིག་འཇུག་བྱ་ཐབས།</translation>
    </message>
    <message>
        <source>Partition too small
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Confirm Custom Installation</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>No root partition
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>No boot partition
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Disk must be greater than 50G or No disk selected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Only one EFI partition is allowed
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 设备上第1分区为 boot, 将设为 ext4;
%1 设备上第2分区为 extend;
%1 设备上第5分区为 /, 将设为 ext4;
%1 设备上第6分区为 backup, 将设为 ext4;
%1 设备上第7分区为 data, 将设为 ext4;
%1 设备上第8分区为 swap, 将设为 swap;
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Full install</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Efi partition number invalid
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 设备上第1分区为 EFI, 将设为 vfat;
%1 设备上第2分区为 boot, 将设为 ext4;
%1 设备上第3分区为 /, 将设为 ext4;
%1 设备上第4分区为 backup, 将设为 ext4;
%1 设备上第5分区为 data, 将设为 ext4;
%1 设备上第6分区为 swap, 将设为 swap;
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Root partition size is greater than 15GiB,
but Huawei machines require greater than 25GiB.
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Efi partition too small
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>BootLoader method %1 inconsistent with the disk partition table 
type %2, cannot have efi partition.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>BackUp partition too small
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The filesystem type FAT16 or FAT32 is not fully-function filesystem,
except EFI partition, other partition not proposed</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>KInstaller::LanguageFrame</name>
    <message>
        <source>Next</source>
        <translation>རྗེས་མ།</translation>
    </message>
    <message>
        <source>Select Language</source>
        <translation>སྒྲིག་འགོད་སྐད་བརྡ།</translation>
    </message>
</context>
<context>
    <name>KInstaller::LicenseFrame</name>
    <message>
        <source>Next</source>
        <translation>རྗེས་མ།</translation>
    </message>
    <message>
        <source>Read License Agreement</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>I have read and agree to the terms of the agreement</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>KInstaller::TimeZoneFrame</name>
    <message>
        <source>Next</source>
        <translation>རྗེས་མ།</translation>
    </message>
    <message>
        <source>Select Timezone</source>
        <translation>དུས་ཚོད་འདེམ་པ།</translation>
    </message>
</context>
<context>
    <name>KInstaller::UserFrame</name>
    <message>
        <source>Next</source>
        <translation>རྗེས་མ།</translation>
    </message>
    <message>
        <source>Detecte fingerprint device.
 Fingerprint entry can be done through [biometric management tool].</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>hostname</source>
        <translation>གཙོ་འཁོར་གྱི་མིང་།</translation>
    </message>
    <message>
        <source>Automatic login on boot</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Your username only letters,numbers,underscore and hyphen are allowed, no more than 32 bits in length.
but start with a lowercase letter</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>username</source>
        <translation>སྤྱོད་མཁན་མིང་།：</translation>
    </message>
    <message>
        <source>Create User</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Two password entries are inconsistent!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Your hostname only letters,numbers,underscore and hyphen are allowed, no more than 64 bits in length.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>enter the password again</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>new password</source>
        <translation>གསང་གྲངས་གསར་པ།</translation>
    </message>
</context>
<context>
    <name>KInstaller::InstallerMainWidget</name>
    <message>
        <source>back</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>quit</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>About to exit the installer, restart the computer.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Installer is about to exit and the computer will be shut down.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>About to exit the installer and return to the trial interface.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>keyboard</source>
        <translation>མཐེབ་གཞོང་།</translation>
    </message>
</context>
<context>
    <name>KInstaller::MiddleFrameManager</name>
    <message>
        <source>next</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>KInstaller::FinishedFrame</name>
    <message>
        <source>Installation Finished</source>
        <translation>སྒྲིག་འཇུག་བྱས་ཚར།</translation>
    </message>
    <message>
        <source>Restart</source>
        <translation>བསྐྱར་སློང་།</translation>
    </message>
</context>
<context>
    <name>KInstaller::InstallingFrame</name>
    <message>
        <source>The system is being installed, please do not turn off the computer</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>KInstaller::CustomPartitiondelegate</name>
    <message>
        <source>%1 MsDos new partition table will be created.
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 GPT new partition table will be created.
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>#%1 partition  on the device %2 will be mounted %3.
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>#%1 partition on the device %2 will be mounted %3.
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>#%1 partition on the device %2 will be deleted.
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>#%1 partition on the device %2 will be formated %3.
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>#%1 partition on the device %2 will be created.
</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>KInstaller::PrepareInstallFrame</name>
    <message>
        <source>Start Installation</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Check it and click [Start Installation]</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>KInstaller::CustomPartitionFrame</name>
    <message>
        <source>Revert</source>
        <translation>སླར་གསོ།</translation>
    </message>
    <message>
        <source>Device for boot loader path:</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>KInstaller::InstallErrorFrame</name>
    <message>
        <source>Installation Failed</source>
        <translation>སྒྲིག་འཇུག་ཕམ་པ།</translation>
    </message>
    <message>
        <source>The computer restarted unexpectedly or encountered an error.
please click "restart" to restart the computer, and then reinstall</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Restart</source>
        <translation>བསྐྱར་སློང་།</translation>
    </message>
</context>
<context>
    <name>KInstaller::InstallingOEMConfigFrame</name>
    <message>
        <source>Progressing system configuration</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>KInstaller::FullPartitionFrame</name>
    <message>
        <source>Please choose custom way to install, disk size less than 50G!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Full disk encryption</source>
        <translation type="unfinished"/>
    </message>
</context>
</TS>